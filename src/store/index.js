import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    personajes: [],
    homicidios: []
  },
  mutations: {
    setPersonajes(state, payload){
      state.personajes = payload;
    },
    setHomicidios(state, payload){
      state.homicidios = payload;
    }
  },
  actions: {
    async getPersonajes({commit}){
      const peticion = await fetch('https://futuramaapi.herokuapp.com/api/v2/characters');
      const data = await peticion.json();
      console.log(data);
      commit('setPersonajes', data);
    },

    async getHomicidios({commit}){
      const peticionHomicidios = await fetch('https://www.datos.gov.co/resource/vtub-3de2.json');
      const data = await peticionHomicidios.json();
      console.log(data);
      commit('setHomicidios', data);
    }
  },
  modules: {
  }
})
